// client.js
var userData;
var userStatus/*true表示已登录*/=false;
var dataStatus/*true表示未修改（已同步）*/=false;
function correctBar(/*根据是否登录给出正确的界面*/){
    if(userStatus){page.className="in";}
    else{page.className="out";}
    if(dataStatus){
        nav.classList.remove("notSynced");
        nav.classList.add("synced");
    }
    else{
        nav.classList.remove("synced");
        nav.classList.add("notSynced");
        title.innerHTML="个人简历";
        fullName.innerHTML="请同步更改";
    }
}
// 提醒数据已经修改
function notifyEdited(){
    if(dataStatus){
        dataStatus=false;
        correctBar();
    }
}
// 警告！调用Ajax方法效率是瓶颈
function sayHello(/*检查当前是否已经登录*/){
    let reqest= new XMLHttpRequest();
    reqest.open("GET","./hello.php",false);
    reqest.send();
    let hello = JSON.parse(reqest.responseText);
    userStatus=hello.status;
    vsitorCounter.innerHTML=hello.count;
}
function verify(){
    loading();// 进入挂起模式
    let reqest= new XMLHttpRequest();
    reqest.onreadystatechange=function(){
    if (reqest.readyState==4 && reqest.status==200){
        var result=JSON.parse(reqest.responseText)
        if(result){
            userStatus=true;
            correctBar();
        }
        else{
            userStatus=false;
            correctBar();
            alert("登录失败！");
        }
        loaded();
    }
    }
    reqest.open("POST","./login.php",true);
    reqest.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    reqest.send("password="+hex_md5(password.value));
    hidePasswordInput(); //隐藏对话框会清除值，所以放在最后
}
function logout(/*注销*/){
    loading();// 进入挂起模式
    let reqest= new XMLHttpRequest();
    reqest.onreadystatechange=function(){
    if (reqest.readyState==4 && reqest.status==200){
        var result=JSON.parse(reqest.responseText)
        if(!result){
            alert("非法访问！");
        }
        userStatus=false;
        if(!dataStatus){getData();}
        correctBar();
        loaded();
    }
    }
    reqest.open("GET","./logout.php",true);
    reqest.send();
}
function setPassword(){
    if(userStatus)/*本地检查*/{
    loading();// 进入挂起模式

    let reqest= new XMLHttpRequest();
    reqest.onreadystatechange=function(){
    if (reqest.readyState==4 && reqest.status==200){
        var result=JSON.parse(reqest.responseText)
        if(result){
            alert("修改密码成功！");}
        else{
            userStatus=false;
            correctBar();
            alert("非法访问，同步失败！");
        }
        loaded();
    }
    }
    reqest.open("POST","./setPassword.php",true);
    reqest.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    reqest.send("password="+hex_md5(password.value));

    }
    hidePasswordInput(); //隐藏对话框会清除值，所以放在最后
}
function setAvatar(){
    if(userStatus)/*本地检查*/{

    loading();// 进入挂起模式
    let fileData=new FormData();
    fileData.append("file",uploader.files[0]);
    let reqest= new XMLHttpRequest();
    reqest.onreadystatechange=function(){
    if (reqest.readyState==4 && reqest.status==200){
        var result=JSON.parse(reqest.responseText)
        if(result.status)/*成功*/{refreshAvatar();}
        else/*失败*/{
            if(result.info==1){
                userStatus=false;
                correctBar();
                alert("非法访问，同步失败！");
            }
            else if(result.info==2){alert("文件不合规，请提供1MB以内的PNG图片！");}
            else /* if(result.info==3) */{alert("未知错误！");}
        }
        loaded();
    }
    }
    reqest.open("POST","./setAvatar.php",true);
    // reqest.setRequestHeader("Content-type","multipart/form-data"); //这行代码是真的假酒害人
    reqest.send(fileData);
    }
    uploader.value="";
}
function setData(/*双向同步数据*/){
    if(userStatus&&(!dataStatus))/*本地检查*/{

    loading();// 进入挂起模式
    //反向绑定数据
    var tmpList=new Array();
    for(let i=0,len=eduList.childElementCount-1;i<len;i++){
        tmpList[i]={
            type: eduList.childNodes[i].childNodes[0].innerHTML,
            place:eduList.childNodes[i].childNodes[1].innerHTML,
            time: eduList.childNodes[i].childNodes[2].innerHTML
        }
    }
    userData={
        fullName    :basicInfo[0].innerHTML,
        birthday    :basicInfo[1].innerHTML,
        sex         :basicInfo[2].innerHTML,
        expectingJob:basicInfo[3].innerHTML,
        major       :basicInfo[4].innerHTML,
        school      :basicInfo[5].innerHTML,
        tel         :basicInfo[6].innerHTML,
        email       :basicInfo[7].innerHTML,
        eduList     :tmpList
    };
    let reqest= new XMLHttpRequest();
    reqest.onreadystatechange=function(){
    if (reqest.readyState==4 && reqest.status==200){
        var result=JSON.parse(reqest.responseText)
        if(result){
            getData();
        }
        else{
            userStatus=false;
            alert("非法访问，同步失败！");
        }
        correctBar();
        loaded();
    }
    }
    reqest.open("POST","./setData.php",true);
    reqest.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    reqest.send("data="+JSON.stringify(userData));

    }
    else if(!userStatus){
        correctBar();
        alert("非法访问，同步失败！");
        loaded();
    }
}
function getData(/*下载同步数据*/){
    if(!dataStatus)/*本地检查*/{
    loading();
    // 获取json
    /*获取信息的Ajax*/let reqest = new XMLHttpRequest();
    reqest.open("GET","./var/data.json?OP="+Math.ceil(Math.random()*10000),false);
    reqest.send();
    /*  接收json   */userData = JSON.parse(reqest.responseText);
    // 本地变量
    /*
        * 以下全都是属性名
        * 0 fullName
        * 1 birthday
        * 2 sex
        * 3 expectingJob
        * 4 major
        * 5 school
        * 6 tel
        * 7 email 
    */
    title.innerHTML=userData.fullName+"的个人简历";
    fullName.innerHTML=userData.fullName;
    var basicInfoI=0;//这个是纯属偷懒用的，后期如果要加属性，就不需要挨个改index值了
    basicInfo[basicInfoI++].innerHTML=userData.fullName;
    basicInfo[basicInfoI++].innerHTML=userData.birthday;
    basicInfo[basicInfoI++].innerHTML=userData.sex;
    basicInfo[basicInfoI++].innerHTML=userData.expectingJob;
    basicInfo[basicInfoI++].innerHTML=userData.major;
    basicInfo[basicInfoI++].innerHTML=userData.school;
    basicInfo[basicInfoI++].innerHTML=userData.tel;
    basicInfo[basicInfoI++].innerHTML=userData.email;    
    
    /*
        * 属性eduList，数组，以下是每个成员的结构
        * 0 type
        * 1 place
        * 2 time
        */
    /*向页面写入教育经历*/
    eduList.innerHTML="";
    for(let i=0,len=userData.eduList.length;i<len;i++){   
        eduList.appendChild(document.createElement("li"));
        eduList.childNodes[i].className="clearfix";

        eduList.childNodes[i].appendChild(document.createElement("span"));
        eduList.childNodes[i].childNodes[0].className="eduType needInput";
        eduList.childNodes[i].childNodes[0].innerHTML=userData.eduList[i].type;

        eduList.childNodes[i].appendChild(document.createElement("span"));
        eduList.childNodes[i].childNodes[1].className="eduPlace needInput";
        eduList.childNodes[i].childNodes[1].innerHTML=userData.eduList[i].place;

        eduList.childNodes[i].appendChild(document.createElement("span"));
        eduList.childNodes[i].childNodes[2].className="eduTime needInput";
        eduList.childNodes[i].childNodes[2].innerHTML=userData.eduList[i].time;

        eduList.childNodes[i].appendChild(document.createElement("div"));
        eduList.childNodes[i].childNodes[3].className="deleteButton opButton";
        eduList.childNodes[i].childNodes[3].innerHTML="删除此项";

        eduList.childNodes[i].appendChild(document.createElement("div"));
        eduList.childNodes[i].childNodes[4].className="insertButton opButton";
        eduList.childNodes[i].childNodes[4].innerHTML="插入项";
    }      
    
    //追加按钮
    eduList.appendChild(document.createElement("li"));
    eduList.lastChild.className="inShow clearfix";
    eduList.lastChild.innerHTML='<div class="appendButton opButton">追加项</div>';
    eduList.lastChild.lastChild.onclick=function(){
        let newItem=eduList.insertBefore(document.createElement("li"),eduList.lastChild);
        newItem.className="clearfix";
        newItem.innerHTML='<span class="eduType needInput">类型</span><span class="eduPlace needInput">地点</span><span class="eduTime needInput">时间</span><div class="deleteButton opButton">删除此项</div><div class="insertButton opButton">插入项</div>';
        setTriggers();
        notifyEdited();
    };
    dataStatus=true;//数据同步完毕，置于未修改状态
    setTriggers();
    loaded();
    }
    correctBar();
}

console.log("client.js已经成功执行到尾部");

//init
refreshAvatar();
sayHello();
getData();
console.log("已经成功初始化");