/*global.js*/

var title=document.getElementsByTagName("title")[0];
var page = document.getElementById("mainPage");
var header = page.getElementsByTagName("header")[0];
var nav = page.getElementsByTagName("nav")[0];
var section = page.getElementsByTagName("section");

var fullName=document.getElementById("name").getElementsByTagName("h1")[0];//页面上的大标题名字
var basicInfo = page.getElementsByClassName("basicKeyValue");/*基础信息dom（ul）*/
var eduList = document.getElementById("eduList");/*教育列表dom*/

var avatarArea = document.getElementById("avatar");
var avatar=avatarArea.getElementsByTagName("img")[0];
var uploader=avatarArea.getElementsByTagName("input")[0];

var inputContainer=document.getElementById("magicInput");
var inputBar=inputContainer.getElementsByTagName("input")[0];
var inputCancel=inputContainer.getElementsByClassName("abandon")[0];
var needInput=page.getElementsByClassName("needInput");

var passwordContainer=document.getElementById("passwordInput");
var passwordCancel=passwordContainer.getElementsByClassName("abandon")[0];
var password=passwordContainer.getElementsByTagName("input")[0];
var passwordCounter=document.getElementById("passwordCounter").childNodes[0];

var vsitorCounter=document.getElementById("vsitorCounter");

// 加载页
var loadingStatus = true;//true-项目在加载 false-加载完毕
var docuLoaded = false;//true-窗体加载完毕 false-窗体正在加载
function loading(){
    if(!loadingStatus){
        document.body.className="loading";
        loadingStatus = true;
    }
}
function loaded(){
    if(loadingStatus&&docuLoaded){
        document.body.className="loaded";
        loadingStatus = false;
    }
}
window.onload=function(){
    docuLoaded=true;
    loaded();
};

// 头像
function refreshAvatar(){avatar.setAttribute("src", "./var/avatar.png?OP="+Math.ceil(Math.random()*10000));}
function avatarError(){avatar.setAttribute("src", "./res/images/error.png");}avatar.setAttribute("onerror", "avatarError()");
avatarArea.onclick=function(){
    if(userStatus){uploader.click();}
};
uploader.addEventListener("change",function(){setAvatar();});

//密码框 虽然简单，考虑到可能要做动画，所以还是写两个专门函数
var passwordShown=false;
function showPasswordInput(){
    passwordShown=true;
    passwordContainer.classList.remove("hiden");
    passwordContainer.classList.add("shown");
    password.focus();
}
function hidePasswordInput(/* 此方法被Client调用，不需要直接调用*/){
    passwordShown=false;
    passwordContainer.classList.remove("shown");
    passwordContainer.classList.add("hiden");
    passwordCounter.innerHTML="0";
    password.value="";//不留值原则
}
function passKeyPress(event){
    if(event.keyCode==13){
        if(userStatus){setPassword();}
        else{verify();}
    }
    else if(event.keyCode==27){
        hidePasswordInput();
    }
}
password.addEventListener("keypress",passKeyPress);
passwordCancel.onclick=hidePasswordInput;
password.oninput=function(){
    passwordCounter.innerHTML=password.value.length+"";
};

//输入框逻辑
var inputShown=false;
var inputReturnPtr;
function showInput(itemName/*为对应顺位的值*/){
    inputShown=true;
    inputReturnPtr=itemName;
    inputBar.value=needInput[inputReturnPtr].innerHTML;
    inputContainer.classList.remove("hiden");
    inputContainer.classList.add("shown");
    inputBar.focus();
    inputBar.select();
}
function hideInput(){
    inputShown=false;
    inputContainer.classList.remove("shown");
    inputContainer.classList.add("hiden");
}
function submitInput(){
    hideInput();
    needInput[inputReturnPtr].innerHTML=inputBar.value;
    notifyEdited();
}
function setTriggers(){
    for(let i=0,len=needInput.length;i<len;i++){
        //编辑项
        needInput[i].onclick=function(){
            if(userStatus&&(!inputShown)){showInput(i);}
        }
    }
    for(let i=0,len=eduList.childElementCount-1;i<len;i++){
        //删除项
        eduList.childNodes[i].childNodes[3].onclick=function(){
            eduList.childNodes[i].remove();
            setTriggers();
            notifyEdited();
        };
        //插入项
        eduList.childNodes[i].childNodes[4].onclick=function(){
            let newItem=eduList.insertBefore(document.createElement("li"),eduList.childNodes[i]);
            newItem.className="inShow clearfix";
            newItem.innerHTML='<span class="eduType needInput">类型</span><span class="eduPlace needInput">地点</span><span class="eduTime needInput">时间</span><div class="deleteButton opButton">删除此项</div><div class="insertButton opButton">插入项</div>';
            setTriggers();
            notifyEdited();
        };
    }
}
function inputKeyPress(event){
    if(event.keyCode==13){submitInput();}
    else if(event.keyCode==27){hideInput();}
}
inputBar.addEventListener("keypress",inputKeyPress);
inputCancel.onclick=hideInput;

var baseTop;
function rollTo(index){
    baseTop=header.offsetHeight+112.6+15;
    let dest=baseTop;
    for(let i=0;i<index;i++){
        dest+=section[i].offsetHeight+15;
    }
    window.scroll({top: dest, behavior: 'smooth'});
}

console.log("global.js已经成功执行到尾部");